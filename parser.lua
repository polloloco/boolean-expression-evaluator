local parse, read, read_S, read_value, read_and

local function make_whitespace(num)
    return string.rep(' ', num)
end

local function error_string(text, str)
    local pos = str:position()
    local str_start = math.max(1, pos - 3)
    local str_end = math.min(#str.internal_data, pos + 3)
    local string = text .. ' at position ' .. pos .. ' near "'
    local whitespace = make_whitespace(#string + (pos - str_start))
    string = string .. str.internal_data:sub(str_start, str_start + str_end - 1) .. '"'
    error(string .. '\n' .. whitespace .. '^', 0)
end

parse = function(str)
    local result = read(str)
    if str:size() ~= 0 then
        error_string('Reached end of expression without reaching end of line', str)
    end
    return result
end

read_and = function(str, lhs)
    local rhs = read_S(str)

    if str:size() == 0 then
       return lhs and rhs
    end

    local next_char = str:peek_char()
    if next_char == '&' then
        str:discard_byte()
        return read_and(str, lhs and rhs)
    end

    return lhs and rhs
end

read_S = function(str)
    if str:size() == 0 then
        error_string('Expected expression but got end of line', str)
    end
    local character = str:peek_char()

    if character == '(' then
        str:discard_byte()
        local inner = read(str)

        if str:size() == 0 then
            error_string('Missing closing parenthesis', str)
        end

        local closing = str:peek_char()
        if closing == ')' then
            str:discard_byte()
            return inner
        else
            error_string('Expected closing parenthesis', str)
        end
    elseif character == '~' then
        str:discard_byte()
        local inner = read_S(str)
        return inner == false
    else
        local val = read_value(str)
        return val
    end
end

read_value = function(str)
    local character = str:peek_char()
    if character == '0' then
        str:discard_byte()
        return false
    elseif character == '1' then
        str:discard_byte()
        return true
    else
        error_string('Expected value, got "' .. character .. '"', str)
    end
end

read = function(str)
    local val = read_S(str)

    if str:size() == 0 then
        return val
    end

    while true do
        if str:size() == 0 then
            break
        end
        local next_char = str:peek_char()
        if next_char == '&' then
            str:discard_byte()

            if str:size() == 0 then
                error_string('Expected expression but got end of line', str)
            end

            local rhs = read_and(str, val)
            val = rhs and val
        elseif next_char == '|' then
            str:discard_byte()

            if str:size() == 0 then
                error_string('Expected expression but got end of line', str)
            end

            local rhs = read(str)
            val = rhs or val
        else
            break
        end
    end

    return val
end

return {
    parse = parse
}