local stream = {}

function stream:new(data)
    data = data or ''
    assert(type(data) == 'string')
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.internal_data = data
    o.pos = 1
    return o
end

function stream:position()
    return self.pos
end

function stream:size()
    return self.internal_data:len() + 1 - self.pos
end

function stream:read_bytes(count)
    assert(self:size() >= count, 'Not enough bytes available on stream. Requested: ' .. count .. ' bytes. Available: ' .. self:size() .. ' bytes')
    local ret = self.internal_data:sub(self.pos, self.pos + count - 1)
    self.pos = self.pos + count
    return ret
end

function stream:read_byte()
    return self:read_bytes(1):byte(1)
end

function stream:peek_bytes(count)
    assert(self:size() >= count, 'Not enough bytes available on stream. Requested: ' .. count .. ' bytes. Available: ' .. self:size() .. ' bytes')
    return self.internal_data:sub(self.pos, self.pos + count - 1)
end

function stream:peek_byte()
    return self:peek_bytes(1):byte(1)
end

function stream:discard_byte()
    self:read_byte()
end

function stream:read_char()
    return self:read_bytes(1)
end

function stream:peek_char()
    return self:peek_bytes(1)
end

return stream