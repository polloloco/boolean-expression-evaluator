local stream = require('stream')
local parser = require('parser')

local function file_exists(file)
    local f = io.open(file, "rb")
    if f then f:close() end
    return f ~= nil
end

local filename = 'boolsche-ausdruecke.txt'
assert(file_exists(filename), 'file "' .. filename .. '" doesnt exist')

do
    local s = stream:new('(~0|~~1&0)')
    local success, res = pcall(parser.parse, s)
    if success then
        print(tostring(res))
    else
        print('Failed:')
        print(res)
    end
end

for line in io.lines(filename) do
    local str = stream:new(line)
    local result = parser.parse(str)
    print(tostring(result))
end